'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var CookieConsentManager = function () {
  /**
   * @constructor
   * @param  {Array<string>} categories - List of cookie categories which should be consentable
   * @param  {string} cookiePrefix='consent_cookie_' - Prefix of the consent cookies
   * @param  {int} cookieExpiresDays=365 - Expiration in days for the consent cookies
   */
  function CookieConsentManager(settings) {
    var _this = this;

    _classCallCheck(this, CookieConsentManager);

    var formTemplate = '';
    var inputs = '';

    if (!settings.title || !settings.description || !Array.isArray(settings.categories) || !settings.categories.length || !settings.labelButtonConsentAll || !settings.labelButtonConsentSelected) {
      throw 'Invalid settings';
    }

    this.consentManager = document.createElement('div');
    this.title = settings.title;
    this.description = settings.description;
    this.detailsLabel = settings.detailsLabel;
    this.cookieExpires = settings.cookieExpiresDays || 365;
    this.cookiePrefix = settings.cookiePrefix || 'ae_cookie_consent_';

    settings.categories.forEach(function (category) {
      inputs += '\n        <div class="cookie-consent-manager__checkbox">\n          <input type="checkbox" id="cookie-consent-manager-' + category.name + '" name="' + category.name + '" value="true" ' + (category.noCookie ? 'data-no-cookie="true"' : '') + ' ' + (category.disabled ? 'disabled' : '') + ' ' + (category.default ? 'checked' : '') + ' />\n          <label for="cookie-consent-manager-' + category.name + '">' + category.title + '</label>\n        </div>\n      ';
    });

    formTemplate = '\n      <div class="cookie-consent-manager__inner">\n        <div class="cookie-consent-manager__headline">\n          ' + this.title + '\n        </div>\n        <form action="">\n          <p class="cookie-consent-manager__description">\n            ' + this.description + '\n          </p>\n          ' + inputs + '\n          <div class="cookie-consent-manager__details">\n            <div class="cookie-consent-manager__details__header">\n              ' + this.detailsLabel + '\n              <span class="ae-icon ae-icon-arrow-down"></span>\n            </div>\n            <div class="cookie-consent-manager__details__content">\n              ' + settings.categories.map(function (category) {
      return '\n                <div class="cookie-consent-manager__details__content__headline">' + category.title + '</div>\n                <div class="cookie-consent-manager__details__content__">' + category.details + '</div>\n              ';
    }).join('') + '\n            </div>\n          </div>\n          <button class="btn consent-all ' + (settings.cssClassStringConsentAllButton || '') + '">\n            ' + settings.labelButtonConsentAll + '\n          </button>\n          <button class="btn consent-selected ' + (settings.cssClassStringConsentSelectedButton || '') + '">\n            ' + settings.labelButtonConsentSelected + '\n          </button>\n        </form>\n      </div>\n    ';

    this.consentManager.className = 'cookie-consent-manager ' + (settings.cssClassString || '');
    this.consentManager.style.display = 'none';
    this.consentManager.insertAdjacentHTML('beforeend', formTemplate);

    document.body.appendChild(this.consentManager);

    this.consentManager.querySelector('.consent-selected').addEventListener('click', function (event) {
      event.preventDefault();
      _this.updateConsent();
    });

    this.consentManager.querySelector('.consent-all').addEventListener('click', function (event) {
      event.preventDefault();
      _this.setConsentAll();
      _this.updateConsent();
    });

    this.consentManager.querySelector('.cookie-consent-manager__details__header').addEventListener('click', function () {
      var detailsContainer = _this.consentManager.querySelector('.cookie-consent-manager__details__content').parentElement;
      if (detailsContainer.className.indexOf('open') !== -1) {
        detailsContainer.className = detailsContainer.className.replace(/\bopen\b/g, "");
      } else {
        detailsContainer.className += ' open';
      }
    });

    if (!this.stateIsValid()) {
      this.show();
    } else {
      this.consentHappened();
    }
  }

  /**
   * Hides the consent manager window
   * @returns {void}
   */


  _createClass(CookieConsentManager, [{
    key: 'hide',
    value: function hide() {
      this.consentManager.style.display = 'none';
    }

    /**
     * Shows the consent manager window after pulling the consent cookies
     * @returns {void}
     */

  }, {
    key: 'show',
    value: function show() {
      this.readCookies();
      this.consentManager.style.display = 'block';
    }

    /**
     * Returns all category checkboxes except the neccessary category checkbox
     * @returns {Array<DomElement>} - Category checkboxes
     */

  }, {
    key: 'getCategories',
    value: function getCategories() {
      var categories = [];
      var checkboxes = this.consentManager.querySelectorAll('input[type=checkbox]');

      for (var i = 0; i < checkboxes.length; i++) {
        if (!checkboxes[i].getAttribute('data-no-cookie')) {
          categories.push(checkboxes[i]);
        }
      }

      return categories;
    }

    /**
     * Checks all consent category checkboxes
     * @returns {void}
     */

  }, {
    key: 'setConsentAll',
    value: function setConsentAll() {
      this.getCategories().forEach(function (category) {
        category.checked = true;
      });
    }

    /**
     * Unchecks all consent categories checkboxes to false
     * @returns {void}
     */

  }, {
    key: 'setConsentNone',
    value: function setConsentNone() {
      this.getCategories().forEach(function (category) {
        category.checked = false;
      });
    }
  }, {
    key: 'updateConsent',
    value: function updateConsent() {
      this.writeCookies();
      this.consentHappened();
      this.hide();
    }

    /**
     * The state is valid if a cookie exists for each category
     * @returns {boolean} - Is the cookie state valid
     */

  }, {
    key: 'stateIsValid',
    value: function stateIsValid() {
      var _this2 = this;

      var valid = true;

      this.getCategories().forEach(function (category) {
        if (_this2.readCookie(category.name) === null) {
          valid = false;
        }
      });

      return valid;
    }

    /**
     * Writes the cookie for each category checkbox based on the checkbox state
     * @returns {void}
     */

  }, {
    key: 'writeCookies',
    value: function writeCookies() {
      var _this3 = this;

      var currentDate = new Date();
      currentDate.setTime(currentDate.getTime() + this.cookieExpires * 60 * 60 * 24 * 1000);
      var expires = 'expires=' + currentDate.toUTCString();
      this.getCategories().forEach(function (category) {
        document.cookie = _this3.cookiePrefix + category.name + '=' + category.checked + ';' + expires + ';path=/';
      });
    }
  }, {
    key: 'readCookies',


    /**
     * Reads the cookie for each category and updates the state of the checkbox
     * @returns {void}
     */
    value: function readCookies() {
      var _this4 = this;

      this.getCategories().forEach(function (category) {
        var categoryConsent = _this4.readCookie(category.name) === 'true';
        _this4.consentManager.querySelector('input[name=' + category.name + ']').checked = categoryConsent;
      });
    }

    /**
     * Returns the value of the cookie specified in the parameter
     * @param {string} category - The category to return the cookie value for
     * @returns {null || string}
     */

  }, {
    key: 'readCookie',
    value: function readCookie(category) {
      var name = '' + this.cookiePrefix + category + '=';
      var decodedCookieString = decodeURIComponent(document.cookie);
      var cookies = decodedCookieString.split(';');

      for (var i = 0; i < cookies.length; i++) {
        var cookie = cookies[i];
        while (cookie.charAt(0) === ' ') {
          cookie = cookie.substring(1);
        }

        if (cookie.indexOf(name) === 0) {
          return cookie.substring(name.length, cookie.length);
        }
      }

      return null;
    }
  }, {
    key: 'consentHappened',


    /**
     * Dispatches the consent happened event on the consent manager element
     * @returns {null || string}
     */
    value: function consentHappened() {
      var event = new CustomEvent('consentHappened', {
        detail: this.getCategories().filter(function (category) {
          return category.checked;
        }).map(function (category) {
          return category.name;
        })
      });
      this.consentManager.dispatchEvent(event);
    }

    /**
     * Returns if a specific category was consented
     * @param  {} category - Category to return consent status for
     * @returns {boolean}
     */

  }, {
    key: 'hasConsent',
    value: function hasConsent(category) {
      return this.stateIsValid() && this.readCookie(category) === 'true';
    }
  }]);

  return CookieConsentManager;
}();

exports.default = CookieConsentManager;
;