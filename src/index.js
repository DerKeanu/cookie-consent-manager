export default class CookieConsentManager {
  /**
   * @constructor
   * @param  {Array<string>} categories - List of cookie categories which should be consentable
   * @param  {string} cookiePrefix='consent_cookie_' - Prefix of the consent cookies
   * @param  {int} cookieExpiresDays=365 - Expiration in days for the consent cookies
   */
  constructor(settings) {
    let formTemplate = '';
    let inputs = '';

    if (!settings.title || !settings.description || !Array.isArray(settings.categories) || !settings.categories.length || !settings.labelButtonConsentAll || !settings.labelButtonConsentSelected) {
      throw('Invalid settings');
    }

    this.consentManager = document.createElement('div');
    this.title = settings.title;
    this.description = settings.description;
    this.detailsLabel = settings.detailsLabel;
    this.cookieExpires = settings.cookieExpiresDays || 365;
    this.cookiePrefix = settings.cookiePrefix || 'ae_cookie_consent_';

    settings.categories.forEach(category => {
      inputs += `
        <div class="cookie-consent-manager__checkbox">
          <input type="checkbox" id="cookie-consent-manager-${category.name}" name="${category.name}" value="true" ${category.noCookie ? 'data-no-cookie="true"' : ''} ${category.disabled ? 'disabled' : ''} ${category.default ? 'checked' : ''} />
          <label for="cookie-consent-manager-${category.name}">${category.title}</label>
        </div>
      `
    });

    formTemplate = `
      <div class="cookie-consent-manager__inner">
        <div class="cookie-consent-manager__headline">
          ${this.title}
        </div>
        <form action="">
          <p class="cookie-consent-manager__description">
            ${this.description}
          </p>
          ${inputs}
          <div class="cookie-consent-manager__details">
            <div class="cookie-consent-manager__details__header">
              ${this.detailsLabel}
              <span class="ae-icon ae-icon-arrow-down"></span>
            </div>
            <div class="cookie-consent-manager__details__content">
              ${settings.categories.map(category => `
                <div class="cookie-consent-manager__details__content__headline">${category.title}</div>
                <div class="cookie-consent-manager__details__content__">${category.details}</div>
              `).join('')}
            </div>
          </div>
          <button class="btn consent-all ${settings.cssClassStringConsentAllButton || ''}">
            ${settings.labelButtonConsentAll}
          </button>
          <button class="btn consent-selected ${settings.cssClassStringConsentSelectedButton || ''}">
            ${settings.labelButtonConsentSelected}
          </button>
        </form>
      </div>
    `;

    this.consentManager.className = `cookie-consent-manager ${settings.cssClassString || ''}`;
    this.consentManager.style.display = 'none';
    this.consentManager.insertAdjacentHTML('beforeend', formTemplate);
  
    document.body.appendChild(this.consentManager);

    this.consentManager.querySelector('.consent-selected').addEventListener('click', (event) => {
      event.preventDefault();
      this.updateConsent();
    });
    
    this.consentManager.querySelector('.consent-all').addEventListener('click', (event) => {
      event.preventDefault();
      this.setConsentAll();
      this.updateConsent();
    });

    this.consentManager.querySelector('.cookie-consent-manager__details__header').addEventListener('click', () => {
      const detailsContainer = this.consentManager.querySelector('.cookie-consent-manager__details__content').parentElement;
      if (detailsContainer.className.indexOf('open') !== -1) {
        detailsContainer.className = detailsContainer.className.replace(/\bopen\b/g, "");
      } else {
        detailsContainer.className += ' open';
      }
    });

    if (!this.stateIsValid()) {
      this.show();
    } else {
      this.consentHappened();
    }
  }

  /**
   * Hides the consent manager window
   * @returns {void}
   */
  hide() {
    this.consentManager.style.display = 'none';
  }

  /**
   * Shows the consent manager window after pulling the consent cookies
   * @returns {void}
   */
  show() {
    this.readCookies();
    this.consentManager.style.display = 'block';
  }

  /**
   * Returns all category checkboxes except the neccessary category checkbox
   * @returns {Array<DomElement>} - Category checkboxes
   */
  getCategories() {
    const categories = [];
    const checkboxes = this.consentManager.querySelectorAll('input[type=checkbox]');

    for (let i = 0; i < checkboxes.length; i++) {
      if (!checkboxes[i].getAttribute('data-no-cookie')) {
        categories.push(checkboxes[i]);
      }
    }

    return categories;
  }

  /**
   * Checks all consent category checkboxes
   * @returns {void}
   */
  setConsentAll() {
    this.getCategories().forEach(category => {
      category.checked = true
    });
  }

  /**
   * Unchecks all consent categories checkboxes to false
   * @returns {void}
   */
  setConsentNone() {
    this.getCategories().forEach(category => {
      category.checked = false
    });
  }

  updateConsent() {
    this.writeCookies();
    this.consentHappened();
    this.hide();
  }

  /**
   * The state is valid if a cookie exists for each category
   * @returns {boolean} - Is the cookie state valid
   */
  stateIsValid() {
    let valid = true;

    this.getCategories().forEach(category => {
      if (this.readCookie(category.name) === null) {
        valid = false;
      }
    });

    return valid;
  }

  /**
   * Writes the cookie for each category checkbox based on the checkbox state
   * @returns {void}
   */
  writeCookies() {
    const currentDate = new Date();
    currentDate.setTime(currentDate.getTime() + (this.cookieExpires * 60 * 60 * 24 * 1000));
    var expires = 'expires=' + currentDate.toUTCString();
    this.getCategories().forEach(category => {
      document.cookie = this.cookiePrefix + category.name + '=' + category.checked + ';' + expires + ';path=/';
    });
  };

  /**
   * Reads the cookie for each category and updates the state of the checkbox
   * @returns {void}
   */
  readCookies() {
    this.getCategories().forEach(category => {
      const categoryConsent = this.readCookie(category.name) === 'true';
      this.consentManager.querySelector(`input[name=${category.name}]`).checked = categoryConsent;
    });
  }

  /**
   * Returns the value of the cookie specified in the parameter
   * @param {string} category - The category to return the cookie value for
   * @returns {null || string}
   */
  readCookie(category) {
    const name = `${this.cookiePrefix}${category}=`;
    const decodedCookieString = decodeURIComponent(document.cookie);
    const cookies = decodedCookieString.split(';');

    for (let i = 0; i < cookies.length; i++) {
      let cookie = cookies[i];
      while (cookie.charAt(0) === ' ') {
        cookie = cookie.substring(1);
      }

      if (cookie.indexOf(name) === 0) {
        return cookie.substring(name.length, cookie.length);
      }
    }

    return null;
  };

  /**
   * Dispatches the consent happened event on the consent manager element
   * @returns {null || string}
   */
  consentHappened() {
    const event = new CustomEvent('consentHappened', {
      detail: this.getCategories().filter(category => { return category.checked; }).map(category => category.name)
    });
    this.consentManager.dispatchEvent(event);
  }

  /**
   * Returns if a specific category was consented
   * @param  {} category - Category to return consent status for
   * @returns {boolean}
   */
  hasConsent(category) {
    return this.stateIsValid() && this.readCookie(category) === 'true';
  }
};